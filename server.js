const express = require('express')
const next = require('next')

const port = parseInt(process.env.PORT, 10) || 5000
const dev = process.env.NODE_ENV !== 'production'
const app = next({dev})
const routes = require('./routes')
const handle = app.getRequestHandler()
// const handler = routes.getRequestHandler(app);
// var cors = require('cors');

var Message = require('./model/message')
var User = require('./model/user')

// const server = express();
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
// const glob = require('glob');

const MONGODB_URI = process.env.MONGODB_URI || 'mongodb://localhost:27017/test'

// const handler = routes.getRequestHandler(app);

// const { join } = require('path');
// const { parse } = require('url');

app.prepare()
    .then(() => {
      const server = express()
        // server.use(handler);

        // Parse application/x-www-form-urlencoded
      server.use(bodyParser.urlencoded({extended: false}))
        // Parse application/json
      server.use(bodyParser.json())

        // Allows for cross origin domain request:
      server.use(function (req, res, next) {
        res.header('Access-Control-Allow-Origin', '*')
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        next()
      })

        // MongoDB
      mongoose.connect(MONGODB_URI, {useMongoClient: true})
        // var db = mongoose.connection;
        // db.on('error', console.error.bind(console, 'connection error:'));

        // API routes
       // const rootPath = require('path').normalize(__dirname + '/..');
       // glob.sync(rootPath + '/server/routes/*.js').forEach(controllerPath => require(controllerPath)(server));

      server.get('/api/messages', function (req, res, next) {
            // console.log('........req.body ++++++', req.body);
            // console.log('........  req.user  >>>>>>      ', req.user);

        Message.find({}, function (err, result) {
          if (err) return next(err)

          console.log('RESULT:  ', result)

          res.json(result)
        })

            // Message.find({user: req.user._id}).populate('user', '-pwd').exec(function (err, result) {
            //     if (err) return next(err);
            //
            //     res.json(result);
            // });
      })

        // app.post('/api/messages', apiRoutes, function (req, res, next) {
        //     console.log('........req.body ++++++', req.body);
        //     console.log('........  req.user  >>>>>>      ', req.user);
        //
        //     // Message.find({}, function (err, result) {
        //     //     if (err) return next(err);
        //     //
        //     //     res.json(result);
        //     // });
        //
        //     Message.find({user: req.user._id}).populate('user', '-pwd').exec(function (err, result) {
        //         if (err) return next(err);
        //
        //         res.json(result);
        //     });
        // });

        // Next.js route
        // server.get('*', (req, res) => {
        //     return handle(req, res)
        // });

        // server.listen(port, function () {
        //     console.log(`App running on http://localhost:${port}/\nAPI running on http://localhost:${port}/api/kittens`)
        // });

      server.get('/a', (req, res) => {
        return app.render(req, res, '/b', req.query)
      })

      server.get('/b', (req, res) => {
        return app.render(req, res, '/a', req.query)
      })

      server.get('/', (req, res) => {
        return app.render(req, res, '/index', req.query)
      })

      server.get('/api', (req, res) => {
        res.sendFile(__dirname + '/data/data.json')
      })

        // server.get('*', (req, res) => {
        //     return handle(req, res)
        // });

        // server.get('*', (req, res) => {
        //     return handle(req, res)
        // });

        // server.get('*', (req, res) => {
        //
        //     const parsedUrl = parse(req.url, true);
        //     const { pathname } = parsedUrl;
        //
        //         // if (pathname === '/service-worker.js') {
        //         //     const filePath = join(__dirname, '.next', pathname)
        //         //     app.serveStatic(req, res, filePath)
        //         // } else {
        //         //     handle(req, res, parsedUrl)
        //         // }
        //
        //     console.log('*******   ',req.url);
        //
        //     return app.render(req, res, '/index', req.query)
        //     //return handle(req, res);
        // });

      server.listen(port, (err) => {
        if (err) throw err
        console.log(`> Ready on http://localhost:${port}`)
      })
    })

// const handler = routes.getRequestHandler(app);
//
// app.prepare()
//     .then(() => {
//         const server = express();
//         server.use(handler);
//
//         server.get('*', (req, res) => {
//             return handle(req, res)
//         });
//
//         server.listen(3000, (err) => {
//             if (err) throw err;
//             console.log('> Ready on http://localhost:3000')
//         })
// });
