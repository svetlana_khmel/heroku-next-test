import React, {Component} from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { addCount } from '../store'

export default class OnlineDetector extends Component {
    constructor () {
        super();

        this.detectOnline = this.detectOnline.bind(this);
        this.state = {
            status: ''
        }
    }

    detectOnline () {
        let status = navigator.onLine;

        this.setState({
            status: status === true ? 'online' : 'offline'
        })
    }

    render () {
        let status = this.state.status;
        //
        // console.log("STATUS, ", status);

        return (
            <div className={status +' online-detector'}>ываываываыва{ status }</div>
        )
    }

    componentDidMount () {
       // this.detectOnline();

        let status = navigator.onLine;

        this.setState({
            status: status === true ? 'online' : 'offline'
        });

        console.log("STATUS 111, ", status);
    }
}