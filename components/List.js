import React, {Component} from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getDataCreator } from '../store'

class List extends Component {
    // add = () => {
    //     this.props.addCount()
    // };

  componentDidMount () {
    this.props.loadData()
  }

  render () {
       // const { count } = this.props
    console.log('STATE   ', this.props)
    return (
      <div>
        <style jsx>{`
                  div {
                    padding: 0 0 20px 0;
                  }
              `}</style>
        <h1>List: <span>sdfsd</span></h1>

        <ul>
          <li>item1</li>
          <li>item2</li>
          <li>item3</li>
        </ul>
        {/* <button onClick={this.add}>Add To Count</button> */}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.data
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    loadData: bindActionCreators(getDataCreator, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(List)
