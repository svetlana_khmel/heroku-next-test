import React, { PropTypes } from 'react'
import {Field, reduxForm} from 'redux-form';
import {validate, warn} from './validation';
import renderField from './renderField';

const SyncValidationRegistrationForm = (props) => {
    const { handleSubmit, pristine, reset, submitting } = props;

    console.log(props);
    return (
        <form onSubmit={handleSubmit}>
            <Field name="username" type="text" component={renderField} label="Username"/>
            <Field name="password" type="password" component={renderField} label="Password"/>
            <Field name="confirmPassword" type="password" component={renderField} label="Confirm Password"/>

            <div>
                <button type="submit" disabled={submitting} className="btn btn-default">Submit</button>
                <button type="button" disabled={pristine || submitting} onClick={reset} className="btn btn-default">Clear Values</button>
            </div>
        </form>
    )
};

export default reduxForm({
    form: 'SyncValidationRegistrationForm',  // a unique identifier for this form
    validate,                // <--- validation function given to redux-form
    warn                     // <--- warning function given to redux-form
})(SyncValidationRegistrationForm)


SyncValidationRegistrationForm.propTypes = {
    handleSubmit: PropTypes.func
};