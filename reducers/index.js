import { combineReducers } from 'redux';
import data from './dataReducer.js';
import user from './loginUserReducer.js';
import { reducer as reduxFormReducer } from 'redux-form';

const rootReducer =  combineReducers({
    data,
    user,
    form: reduxFormReducer, // mounted under "form"
});

export default rootReducer;