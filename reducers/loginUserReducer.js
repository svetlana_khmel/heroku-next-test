import * as types from '../actionTypes';

let initialState = {};

export default (state = initialState, action) => {
    switch (action.type) {

        case types.LOGIN_USER:
            return {
                ...state, ...action.payload
            };

        case types.REGISTER_USER:
            return {
                ...state, ...action.payload
            };

        case types.LOGOUT:
            return {};

        default:
            return state
    }
}