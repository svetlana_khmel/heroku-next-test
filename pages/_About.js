import React, { Component } from 'react'
import Navigation from '../components/navigation'
import Header from './Header'

class About extends Component {
  constructor (props) {
    super(props)
  }

  render () {
    return (
      <div>
        <Header history={this.props.history} />
        <Navigation />
                About page
      </div>)
  }
}

export default About
