import React, {Component} from 'react'
import Header from './Header'
import Navigation from '../components/navigation'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import OnlineDetector from '../components/OnlineDetector'
import { loginUser, registerUser, deleteArticle, addArticle, editArticle, initStore } from '../store'
import withRedux from 'next-redux-wrapper'
import Page from '../components/fakePage'
import List from '../components/List'

class App extends Component {
  constructor (props) {
    super(props)

    this.getSessionToken = this.getSessionToken.bind(this)

    this.state = {
      isLoggedIn: false
    }
  }

  componentDidMount () {
    let token = this.getSessionToken()

    if (this.state.isLoggedIn === false) {
      this.props.history.push('/login')
    } else {
      if (token) {
        this.setState({
          isLoggedIn: true
        })
      }
    }
  }

  getSessionToken () {
    if (typeof (Storage) !== 'undefined') {
      return sessionStorage.getItem('access_token')
    } else {
            // Sorry! No Web Storage support..
    }
  }

  render () {
    const isLoggedIn = this.state.isLoggedIn
    let header = null

    if (isLoggedIn == true) {
      header = <Header history={this.props.history} />
    }

    return (
      <div className='messenger-container'>
        <OnlineDetector />
        <Navigation />
        <List />

        {header}

        {isLoggedIn == true &&
        <div>
          <Navigation />
        </div>
                }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
       // data: state.data,
    user: state.user.user
        // newUser: state.newUser.user
  }
}

// const mapDispatchToProps = (dispatch) => {
//     return {
//         loginUser: bindActionCreators(loginUser, dispatch),
//         registerUser: bindActionCreators(registerUser, dispatch),
//         deleteArticle: bindActionCreators(deleteArticle, dispatch),
//         addArticle: bindActionCreators(addArticle, dispatch),
//         editArticle: bindActionCreators(editArticle, dispatch),
//     }
// };

const mapDispatchToProps = (dispatch) => {
  return {
    onClick: () => {
      dispatch(loginUser())
    }
  }
}

export default withRedux(initStore, null, mapDispatchToProps)(App)
