import React, {Component} from 'react';
import Header from './Header';
import SearchInput from '../components/searchInput';

import Navigation from '../components/navigation';
import Category from '../components/category';

import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as actions from "../actions/dataActionCreators";

class App extends Component {
    constructor(props) {
        super(props);

        this.editData = this.editData.bind(this);

        this.getInputData = this.getInputData.bind(this);
        this.getList = this.getList.bind(this);
        this.handleFormChange = this.handleFormChange.bind(this);
        this.storeSession = this.storeSession.bind(this);
        this.getSessionToken = this.getSessionToken.bind(this);
        this.removeFullArticle = this.removeFullArticle.bind(this);
        this.renderArticle = this.renderArticle.bind(this);
        this.doSearch = this.doSearch.bind(this);
        this.setEditableFields = this.setEditableFields.bind(this);
        this.translateArticle = this.translateArticle.bind(this);


        this.expandedText = this.expandedText.bind(this);
        this.cancelEdit = this.cancelEdit.bind(this);
        this.showCategory = this.showCategory.bind(this);
        this.setLocalStorage = this.setLocalStorage.bind(this);

        this.state = {
            message: '',
            fullArticle: {
                title: '',
                articleRaw: '',
                article: [],
                translation: [],
                category: ''
            },

            editableArticle: {
                title: '',
                articleRaw: '',
                article: [],
                translation: [],
                category: ''
            },

            messages: [],
            isLoggedIn: false,
            expanded: false,
            translation: false,
            query: '',
            filteredMessages: [],
            editMode: false
        }
    }

    componentDidMount() {

        if(this.props.data.length === 0) {
            let token = this.getSessionToken();

            if (token) {
                this.setState({
                    isLoggedIn: true
                });

                this.getList();
            }
        } else {
            this.setData();
        }
    }

    componentWillReceiveProps (nextProps) {
        if ( nextProps.data.length!==0 && nextProps.data.length !== this.props.data) {

            this.setState({
                messages: nextProps.data,
                filteredMessages: nextProps.data
            });

            this.setLocalStorage(nextProps.data);
        }
    }

    setData () {
        this.setState({
            messages: this.props.data,
            filteredMessages: this.props.data
        });

        this.setLocalStorage(this.props.data);
    }

    doSearch(queryText) {
        let queryResult = [];

        this.state.messages.forEach(function (person) {
            if (person.data.title.toLowerCase().indexOf(queryText.toLowerCase()) != -1)
                queryResult.push(person);
        });

        this.setState({
            query: queryText,
            filteredMessages: queryResult
        });
    }

    showCategory(queryText) {
        let queryResult = [];

        if (queryText === 'all') {
            queryResult = this.state.messages;

            this.setState({
                query: queryText,
                filteredMessages: queryResult
            });
        }

        this.state.messages.forEach((person) => {
            if (person.data.category.toLowerCase().indexOf(queryText.toLowerCase()) != -1) {
                queryResult.push(person);
            }
        });

        this.setState({
            query: queryText,
            filteredMessages: queryResult
        });
    }

    setEditableFields(el) {
        this.setState({
            fullArticle: {
                title: el.title,
                article: el.article,
                translation: el.translation,
                category: el.category
            }
        });
    }

    cancelEdit() {
        this.setState({
            expanded: false
        });
    }

    expandedText(el, index, id) {
        this.child = id;

        this.setState({
            expanded: true
        });

        this.setEditableFields(el, index, id)
    }

    renderArticle(el, index, id) {
        return <div>
                    <div className="edit-block">
                        <div data-id={id} className="btn edit" onClick={() => {
                            this.expandedText(el, index, id)
                        }}>Edit
                        </div>
                        <div data-id={id} className="btn remove" onClick={this.removeFullArticle}>Remove</div>
                        <div data-id={id} className="btn translate" onClick={this.translateArticle}>Translate</div>
                    </div>

                    <div className={'text-block ' + (this.state.expanded && id === this.child ? "hidden" : '')}>
                        <div className={'title'} key={'nn' + index} dangerouslySetInnerHTML={{__html: el.title}} onClick={this.openArticle}></div>
                        <div className={'article'  + (this.state.translation === true? ' hidden transtation-open':'')} key={'nn-article' + index}
                             dangerouslySetInnerHTML={{__html: typeof el.article === "string" ? el.article : el.article.join("<br>")}}
                             onClick={this.openArticle}></div>
                        <div className={'article-translation' + (this.state.translation === true? ' transtation-block-open':'')} key={'nn-translation' + index}
                             dangerouslySetInnerHTML={{__html: typeof el.translation === "string" ? el.translation : el.translation.join("<br>")}}
                             onClick={this.openArticle}></div>
                        <div className={'category'} key={'nn-category' + index} dangerouslySetInnerHTML={{__html: el.category}}
                             onClick={this.openArticle}></div>
                    </div>
        </div>
    }

    getMoreTextDiv(el, index, id) {

        if (this.state.expanded && id === this.child) {
            return <div data-id={id} key={'editable' + index} id={el._id}>
                <div className="title-edit">
                    <label>Title:</label>
                    <textarea rows={'1'} className={'textarea'} data-id={id} data-attr="title" onKeyUp={this.getInputData}
                              type="text">{el.title}</textarea>
                </div>
                {/* <div className="original">
                 <legend>original</legend>
                 <input data-id={id} data-attr="original" type="file" onChange={this.handleFileUpload} />
                 </div>*/}
                <div className="article-body-edit">
                    <label> Article: </label>
                    <textarea rows={'15'} className={'textarea'} data-id={id} data-attr="article" placeholder={'Some lyrics\n' +
                    'may be here...'}
                              onKeyUp={this.getInputData}>{typeof this.state.fullArticle.article === 'string' ? this.state.fullArticle.article : this.state.fullArticle.article.join("\n")}</textarea>
                </div>
                <div className="translation-body-edit">
                    <label>Translation:</label>
                    <textarea rows={'15'} className={'textarea'} data-id={id} data-attr="translation" placeholder={'Some translation\n' +
                    'may be here...'}
                              onKeyUp={this.getInputData}>{typeof el.translation === 'string' ? el.translation : el.translation.join("\n")}</textarea>
                </div>
                <div className="category-body-edit">
                    <label>Category:</label>
                    <textarea className={'textarea category-textarea'} data-id={id} data-attr="category" placeholder={'Add tags like: Favorites, Madonna'} onKeyUp={this.getInputData}>{el.category}</textarea>
                </div>

                {/*<div className="minus">*/}
                    {/*<legend>minus</legend>*/}
                    {/*<input data-id={id} data-attr="minus" type="file" onChange={this.handleFileUpload}/>*/}
                {/*</div>*/}
                <button data-id={id} className="btn send" onClick={this.editData}>Save</button>
                <button data-id={id} className="btn cancel" onClick={this.cancelEdit}>Cancel</button>
            </div>
        } else {
            return null;
        }
    }

    getList() {
        const token = this.getSessionToken();

        this.props.loadData(token);
    }

    setLocalStorage(data) {
        console.log("SET LOCAL__", data);
    }

    getLocalStorage(data) {
        console.log("SET LOCAL__", data);
    }

    editData() {
        const token = this.getSessionToken();
        const articleId = this.child;

        const data = {
            title: this.state.fullArticle.title,
            article: /\n/.test(this.state.fullArticle.article) ? this.state.fullArticle.article.split(/\n/) : this.state.fullArticle.article,
            translation: /\n/.test(this.state.fullArticle.translation) ? this.state.fullArticle.translation.split(/\n/) : this.state.fullArticle.translation,
            category: this.state.fullArticle.category,
            articleId
        };

        this.props.editArticle(token, articleId, data);
        this.cancelEdit();
    }

    storeSession(token) {
        if (typeof(Storage) !== "undefined") {
            sessionStorage.setItem("access_token", token);
        } else {
            // Sorry! No Web Storage support..
        }
    }

    getSessionToken() {
        if (typeof(Storage) !== "undefined") {
            return sessionStorage.getItem("access_token");
        } else {
            // Sorry! No Web Storage support..
        }
    }

    getInputData(event) {
        let title = "";
        let article = [];
        let translation = [];
        let category = '';
        let name = event.target.dataset.attr;

        if (name === 'title') {
            title = event.target.value;
            article = this.state.fullArticle.article;
            translation = this.state.fullArticle.translation;
            category = this.state.fullArticle.category;
        }

        if (name === 'article') {
            console.log('event.target.value', event.target.value);

            let rawData = event.target.value;

            article = rawData;
            console.log('SPLIT', article);
            title = this.state.fullArticle.title;
            translation = this.state.fullArticle.translation;
            category = this.state.fullArticle.category;
        }

        if (name === 'translation') {
            console.log('event.target.value', event.target.value);

            let rawData = event.target.value;

            //translation = rawData.split(/\n/);
            translation = rawData;

            title = this.state.fullArticle.title;
            article = this.state.fullArticle.article;
            category = this.state.fullArticle.category;
        }

        if (name === 'category') {

            category = event.target.value;

            title = this.state.fullArticle.title;
            article = this.state.fullArticle.article;
            translation = this.state.fullArticle.translation;
        }

        this.setState({
            fullArticle: {
                title,
                article,
                translation,
                category
            }
        });
    }

    handleFormChange(event) {
        let data = {};

        data[event.target.dataset.attr] = event.target.value;

        this.setState(
            data
        );
    }

    removeFullArticle(event) {
        let id = event.target.dataset.id;

        this.props.deleteArticle(id);
    }

    translateArticle (event) {

        let id = event.target.dataset.id;

        let translation = this.state.translation;

        translation === false ? translation = true: translation = false;

        this.setState({
            translation
        })
    }

    render() {
        return (
            <div className="messenger-container">
                <Header history={this.props.history}/>
                <Navigation parentprops={this.props} history={this.props.history}/>
                {
                    this.state.messages.length != 0 &&

                    <div>
                        <Category showCategory={this.showCategory} messages={this.state.messages}/>
                        <SearchInput query={this.state.query} doSearch={this.doSearch}/>
                    </div>
                }

                <div>
                    {this.state.messages &&
                        this.state.filteredMessages.map((el, index) => {
                            return <div className="full-article" key={'fullarticle' + index}>
                                {this.renderArticle(el.data, index, el._id)}
                                {this.getMoreTextDiv(el.data, index, el._id)}
                            </div>
                        })
                    }
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        data: state.data,
        user: state.user
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        loadData: actions.loadData,
        deleteArticle: actions.deleteArticle,
        editArticle: actions.editArticle
    }, dispatch);
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
