import React from 'react'
// import Layout from 'layouts/Main'
import { getPosts } from 'api/data'
import PostItem from 'components/Post/PostItem'
import Nav from 'components/Nav'

// import Post from 'components/Post'

const IndexPage = ({ posts }) =>
  <div>
    <Nav />

    <ul>
      {posts.map(p => <PostItem key={p.data.id} post={p} />)}
    </ul>
  </div>

/* const IndexPage = ({ posts }) =>
  <div>
    <ul>
      {posts.map(p => <div className={'article'} key={p.data.id}>
        <div className={'title'} key={p.data.id}>{p.data.title}</div>
        <div className={'post'}>{p.data.article[0]}</div>
        <div className={'translation'}>{p.data.translation[0]}</div>
        <div className={'category'}>{p.data.category}</div>
      </div>)}
    </ul>
  </div> */

IndexPage.getInitialProps = async ({ req }) => {
  const res = await getPosts()
  const json = await res.json()

    // console.log('*******    ', json);
  return { posts: json }
}

export default IndexPage
