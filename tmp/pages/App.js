import React, {Component} from 'react';
import Header from './Header';
import Navigation from '../components/navigation';

import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as actions from "../actions/dataActionCreators";
import OnlineDetector from "./OnlineDetector";

class App extends Component {

    constructor(props) {
        super(props);

        this.getSessionToken = this.getSessionToken.bind(this);

        this.state = {
            isLoggedIn: false
        }
    }

    componentDidMount() {
        let token = this.getSessionToken();

        if (this.state.isLoggedIn === false) {
            this.props.history.push('/login');
        } else {

            if (token) {
                this.setState({
                    isLoggedIn: true
                });
            }

        }
    }

    getSessionToken() {

        if (typeof(Storage) !== "undefined") {
            return sessionStorage.getItem("access_token");
        } else {
            // Sorry! No Web Storage support..
        }
    }

    render() {
        const isLoggedIn = this.state.isLoggedIn;
        let header = null;

        if (isLoggedIn == true) {
            header = <Header history={this.props.history}/>
        }

        return (
            <div className="messenger-container">
                <OnlineDetector />

                {header}

                {isLoggedIn == true &&
                <div>
                    <Navigation/>
                </div>
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        data: state.data,
        user: state.user.user,
        //newUser: state.newUser.user
    };
};

export default connect(
    mapStateToProps,
    dispatch => bindActionCreators(actions, dispatch)
)(App);


// import React from 'react'
// import { bindActionCreators } from 'redux'
// import { initStore, startClock, addCount, serverRenderClock } from '../store'
// import withRedux from 'next-redux-wrapper'
// import Page from '../components/Page'
//
// class Counter extends React.Component {
//     static getInitialProps ({ store, isServer }) {
//         store.dispatch(serverRenderClock(isServer))
//         store.dispatch(addCount())
//
//         return { isServer }
//     }
//
//     componentDidMount () {
//         this.timer = this.props.startClock()
//     }
//
//     componentWillUnmount () {
//         clearInterval(this.timer)
//     }
//
//     render () {
//         return (
//             <Page title='Index Page' linkTo='/other' />
//         )
//     }
// }
//
// const mapDispatchToProps = (dispatch) => {
//     return {
//         addCount: bindActionCreators(addCount, dispatch),
//         startClock: bindActionCreators(startClock, dispatch)
//     }
// }

//export default withRedux(initStore, null, mapDispatchToProps)(Counter)


