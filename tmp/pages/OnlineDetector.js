import React, { Component } from 'react';

export default class OnlineDetector extends Component {
    constructor () {
        super();

        this.detectOnline = this.detectOnline.bind(this);
        this.state = {
            status: ''
        }
    }

    detectOnline () {
        let status = navigator.onLine;

        this.setState({
            status: status === true ? 'online' : 'offline'
        })
    }

    render () {
        let status = this.state.status;

        return (
            <div className={status +' online-detector'}>{ status }</div>
        )
    }

    componentDidMount () {
        this.detectOnline();
    }
}