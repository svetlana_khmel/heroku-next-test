import React from 'react'
import { bindActionCreators } from 'redux'
import { initStore, startClock, addCount, serverRenderClock } from '../store'
import withRedux from 'next-redux-wrapper'
import Page from '../components/Page'

class Counter extends React.Component {
    static getInitialProps ({ store, isServer }) {
        store.dispatch(serverRenderClock(isServer))
        store.dispatch(addCount())

        return { isServer }
    }

    componentDidMount () {
        this.timer = this.props.startClock()
    }

    componentWillUnmount () {
        clearInterval(this.timer)
    }

    render () {
        return (
            <Page title='Index Page' linkTo='/other' />
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addCount: bindActionCreators(addCount, dispatch),
        startClock: bindActionCreators(startClock, dispatch)
    }
}

export default withRedux(initStore, null, mapDispatchToProps)(Counter);


//
//
//
// import React from 'react'
// import { bindActionCreators } from 'redux'
// import { initStore, startClock, addCount, serverRenderClock } from '../store'
// import withRedux from 'next-redux-wrapper'
// import Page from '../components/Page'
//
// class Counter extends React.Component {
//     static getInitialProps ({ store, isServer }) {
//         store.dispatch(serverRenderClock(isServer))
//         store.dispatch(addCount())
//
//         return { isServer }
//     }
//
//     componentDidMount () {
//         this.timer = this.props.startClock()
//     }
//
//     componentWillUnmount () {
//         clearInterval(this.timer)
//     }
//
//     render () {
//         return (
//             <Page title='Index Page' linkTo='/other' />
//         )
//     }
// }
//
//
//
// import React from 'react';
// import ReactDOM from 'react-dom';
// import App from './App';
// import About from './About';
// import NewPost from './NewPost';
// import List from './List';
// import Login from './Login';
//
// import {
//     BrowserRouter as Router,
//     Route,
//     Link
// } from 'react-router-dom';
//
// import { Provider } from 'react-redux';
// import configureStore from '../configureStore';
//
// const store = configureStore();
//
// ReactDOM.render(
//     <Provider store={store}>
//         <Router>
//             <div>
//                 <Route path='/' component={App} />
//                 <Route path='/about' component={About}/>
//                 <Route path='/login' component={Login}/>
//                 <Route path='/new' component={NewPost}/>
//                 <Route path='/list' component={List}/>
//             </div>
//         </Router>
//     </Provider>
// );
//
