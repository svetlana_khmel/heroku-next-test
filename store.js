import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunkMiddleware from 'redux-thunk'
import data from './data'
// import actionTypes from './actionTypes'

const initialState = {
  data: [],
  user: {}
}

export const actionTypes = {
  GET_MESSAGES: 'ARTICLES::GET_MESSAGES',
  ADD_ARTICLE: 'ARTICLES::ADD_ARTICLE',
  REMOVE_ARTICLE: 'ARTICLES::REMOVE_ARTICLE',
  EDIT_ARTICLE: 'ARTICLES::EDIT_ARTICLE',
  LOGIN_USER: 'USER::LOGIN_USER',
  REGISTER_USER: 'USER::REGISTER_USER'
}

// REDUCERS
export const reducerUser = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_MESSAGES:
      return [...state, ...action.payload]

    case actionTypes.ADD_ARTICLE:
      return [...state, action.payload]

    case actionTypes.REMOVE_ARTICLE:
      return state.filter((article) => {
        if (article._id !== action.id) {
          return article
        }
      })

    case actionTypes.EDIT_ARTICLE:
      return state.map((article, index) => {
        if (article._id !== action.data._id) {
                    // This isn't the item we care about - keep it as-is
          return article
        }
                // Otherwise, this is the one we want - return an updated value

        console.log('...edit ..', {...article, ...action.data})
        console.log('...state ..', ...state)

        return {
          ...article,
          ...action.data
        }
      })

    default:
      return state
  }
}

// ACTIONS

export const getDataCreator = (data) => {
  if (typeof Storage !== 'undefined') {
    localStorage.setItem('lyricsData', JSON.stringify(data))
  }
  return {
    type: actionTypes.GET_MESSAGES,
    payload: {data: 'some data'}
  }
}

export const addArticleCreator = (data) => {
  debugger
  return {
    type: actionTypes.ADD_ARTICLE,
    payload: data
  }
}

export const removeArticle = (id) => {
  return {
    type: actionTypes.REMOVE_ARTICLE,
    id
  }
}

export const editArticleCreator = (data) => {
  return {
    type: actionTypes.EDIT_ARTICLE,
    data
  }
}

export const setUser = (data) => {
  return {
    type: actionTypes.LOGIN_USER,
    payload: data
  }
}

export const logOutCreator = () => {
  return {
    type: types.LOGOUT
  }
}

export const createUser = (data) => {
  return {
    type: actionTypes.REGISTER_USER,
    payload: data
  }
}

// export const loadData = (token) => {
//     return (dispatch, getState) => {
//         return data.getMessages(token)
//             .then((response)=>response.json())
//             .then((data)=> dispatch(getDataCreator(data)))
//             .catch((err)=> console.log(err));
//     };
// };

export const loadData = (token) => {
  console.log('get data')
  return (dispatch, getState) => {
    return data.getMessages(token)
            .then((response) => response.json())
            .then((data) => dispatch(getDataCreator(data)))
            .catch((err) => console.log(err))
  }
}

export const loginUser = (userdata) => {
  return (dispatch) => {
    return data.loginUser(userdata)
            .then((response) => response.json())
            .then((data) => {
              console.log('ddd', data)
              dispatch(setUser(data))
            })
            .catch((err) => console.log(err))
  }
}

export const registerUser = (userdata) => {
  return (dispatch) => {
    return data.registerUser(userdata)
            .then((response) => response.json())
            .then((data) => dispatch(createUser(data)))
            .catch((err) => console.log(err))
  }
}

export const deleteArticle = (id) => {
  return (dispatch) => {
    return data.deleteArticle(id)
            .then((response) => response.json())
            .then((data) => dispatch(removeArticle(id)))
            .catch((err) => console.log(err))
  }
}

export const editArticle = (token, id, article) => {
  return (dispatch) => {
    return data.editArticle(token, id, article)
            .then((response) => response.json())
            .then((data) => dispatch(editArticleCreator(data)))
            .catch((err) => console.log(err))
  }
}

export const addArticle = (token, id, article) => {
  return (dispatch) => {
    return data.addArticle(token, id, article)
            .then((response) => response.json())
            .then((data) => dispatch(addArticleCreator(data)))
            .catch((err) => console.log(err))
  }
}

export const initStore = (initialState = initialState) => {
  return createStore(reducerUser, initialState, composeWithDevTools(applyMiddleware(thunkMiddleware)))
}

// const exampleInitialState = {
//     lastUpdate: 0,
//     light: false,
//     count: 0
// }
//
// export const actionTypes = {
//     ADD: 'ADD',
//     TICK: 'TICK'
// }
//
// // REDUCERS
// export const reducer = (state = exampleInitialState, action) => {
//     switch (action.type) {
//         case actionTypes.TICK:
//             return Object.assign({}, state, { lastUpdate: action.ts, light: !!action.light })
//         case actionTypes.ADD:
//             return Object.assign({}, state, {
//                 count: state.count + 1
//             })
//         default: return state
//     }
// }
//
// // ACTIONS
// export const serverRenderClock = (isServer) => dispatch => {
//     return dispatch({ type: actionTypes.TICK, light: !isServer, ts: Date.now() })
// }
//
// export const startClock = () => dispatch => {
//     return setInterval(() => dispatch({ type: actionTypes.TICK, light: true, ts: Date.now() }), 800)
// }
//
// export const addCount = () => dispatch => {
//     return dispatch({ type: actionTypes.ADD })
// }
//
// export const initStore = (initialState = exampleInitialState) => {
//     return createStore(reducer, initialState, composeWithDevTools(applyMiddleware(thunkMiddleware)))
// }
